" Todo:
" [ ] I believe repeated use of au FileType is inefficient
" [ ] Automatically close tagbar and nerdtree with central window closes
" [ ] Install some sort of git manager
" [ ] Some sort of improved status line
" [ ] keyboard shortcuts for Tagbar
" [ ] Autocompletion
" [ ] Do no open NERDTree, Tagbar, etc if terminal is too small
" [ ] Refine prose navigation keys (see move cursor by display lines link)
" [ ] Improve indenting for lists (see reddit link for inspo)
"
" References:
" - https://github.com/junegunn/vim-plug
" - https://github.com/preservim/tagbar
" - https://github.com/amix/vimrc
" - https://github.com/preservim/nerdtree
" - https://vimhelp.org/map.txt.html#%3CLeader%3E
" - `vimtutor`
" - <C-w> + hjkl/arrow keys switches between windows
" - https://github.com/vim-airline/vim-airline
" - https://github.com/qadzek/link.vim
" - https://github.com/gerw/vim-latex-suite
" - https://www.vim.org/scripts/script.php?script_id=6102 (link.vim plugin)
" - https://vim.fandom.com/wiki/Move_cursor_by_display_lines_when_wrapping
" - https://old.reddit.com/r/vim/comments/an30mk/how_do_i_indent_a_wrap_by_more_than_the_start_of/

syntax on
set mouse=a

" Load plugins with vim-plug
call plug#begin('~/.local/share/nvim/plugged')
    "Plug 'Valloric/YouCompleteMe'  "Needs Python update or something
    Plug 'https://github.com/preservim/nerdtree'
    Plug 'https://github.com/davidoc/taskpaper.vim'
    Plug 'https://github.com/junegunn/goyo.vim'
    Plug 'https://github.com/preservim/tagbar'
call plug#end()

" Keyboard Mappings
    " Show/hide nerdtree
    map <C-n> :NERDTreeToggle<CR>

" File type settings
    " Java
    au FileType java set expandtab ts=4 sw=4 ai             "tab = 4 spaces
    au FileType java set number                             "Show numbers
    au BufNewFile *.java 0r ~/Dropbox/Templates/skel.java   "Template

    " Markdown
    au FileType markdown,md set expandtab ts=2 sw=2 ai      "tab = 2 spaces
    au FileType markdown,md silent set linebreak breakat    "break lines at whitespace
    au FileType markdown,md set linebreak breakindent       "Indent same amount after break
    au FileType markdown,md nnoremap k      gk
    au FileType markdown,md nnoremap j      gj
    au FileType markdown,md nnoremap <Up>   gk
    au FileType markdown,md nnoremap <Down> gj
    au FileType markdown,md inoremap <Up>   <C-o>gk
    au FileType markdown,md inoremap <Down> <C-o>gj

    " Shell scripts
    au FileType sh set expandtab ts=4 sw=4 ai               "tab = 2 spaces
    au FileType sh set number                               "Show numbers
    au FileType sh NERDTree                                 "open file browser
    au Filetype sh TagbarOpen                               "open ctags window

    " Taskpaper
    au FileType taskpaper silent set linebreak breakat      "break lines at whitespace
    au FileType taskpaper set linebreak breakindent         "Indent same amount after break

    " Vim configs
    au FileType vim set expandtab ts=4 sw=4 ai              "tab = 2 spaces
    au FileType vim set number                              "Show numbers

" File path settings
    " Notes
    "au BufRead,BufNewFile $HOME/Dropbox/Notes/* NERDTree

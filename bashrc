# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

settings="$HOME/.config/diwesser/device-id"

# History
HISTSIZE=1000
HISTFILESIZE=10000
HISTTIMEFORMAT="%y-%m-%d %T "
HISTIGNORE="[ \t]*" # Exclude lines begining with a space from history
shopt -s histappend

# Use vim as default editor
VISUAL=vim; EDITOR=vim
export VISUAL EDITOR

# Import my configs
# TODO: Replace variable extraction with bash parameter expansion
if [ -f $HOME/.config/diwesser/device-id ]; then
    SYNC_DIR=$(grep -iw '^\s*sync_dir:' $settings | cut -d: -f2 | xargs)
    #prompt=$()
fi

if [ -f "$HOME/.bash_aliases" ] ; then
    . "$HOME/.bash_aliases"
fi

# Using a ternary oerator this time cause we're fancy
test -s ~/.alias && . ~/.alias || true

# Add scripts etc to PATH
if [ -d "$HOME/.local/bin" ] ; then
    export PATH="$PATH:$HOME/.local/bin"
fi

if [ -d "$HOME/bin" ] ; then
    export PATH="$PATH:$HOME/bin"
fi

prompt_parse_git_branch() {
    local branch_short=$(git branch 2> /dev/null | sed -e '/^[^*]/d' | sed 's/^[*] //g' | cut -c -30)
    echo "$branch_short"
}

prompt_git_status() {
    # Outputs a series of indicators based on the status of the
    # working directory:
    # + changes are staged and ready to commit
    # X unstaged changes are present
    # ? untracked files are present
    # (up arrow) local branch ahead of remote
    # (down arrow) local branch behind remote
    # S changes have been stashed
    local status="$(git status --branch --porcelain 2>/dev/null)"
    local output=''
    [[ -n $(egrep '^[MADRC]' <<<"$status") ]] && output="${output}+"
    [[ -n $(egrep '^.[MD]' <<<"$status") ]] && output="${output}X"
    [[ -n $(egrep '^\?\?' <<<"$status") ]] && output="${output}?"
    [[ -n $(fgrep 'behind' <<<"$status") ]] && output="${output}↓ "
    [[ -n $(fgrep 'ahead' <<<"$status") ]] && output="${output}↑ "
    [[ -n $(git stash list) ]] && output="${output}S"
    [[ -z $output ]] && output='✓ '
    output="[$output]"
    echo $output
}

prompt_git_prompt() {
    # First, get the branch name...
    local branch=$(prompt_parse_git_branch)
    # Empty output? Then we're not in a Git repository, so bypass the rest
    # of the function, producing no output
    if [[ -n $branch ]]; then
        local state=$(prompt_git_status)
        # Now output the actual code to insert the branch and status
        echo "$branch $state "
    fi
}

# <blue><\u><white><@><blue><\h> <purple> <\W (CURRENT DIR)> <green><git status> <white>
PS1='\[\033[01;34m\]\u\[\033[0m\]@\[\033[01;34m\]\h \[\033[01;35m\]\W \[\033[01;32m\]$(prompt_git_prompt)\[\033[01;00m\]\[\033[0m\]'

#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\] \[\033[01;93m\]$(prompt_git_prompt)\[\033[00m\]\$ '

#!/bin/bash
script_path="$(dirname $0)"
printf "$script_path\n"

# Mac
if [[ $(uname -s) == Darwin ]] ; then
    # Remove any existing dotfile
    rm $HOME/.bash_profile

    # Link new dotfiles
    ln -s $script_path/bashrc $HOME/.bash_profile
    # .vimrc location dependent on Neovim
    if [[ $(command -v nvim) ]] ; then
        rm $HOME/.config/nvim/init.vim
        ln $script_path/vimrc $HOME/.config/nvim/init.vim
    else
	rm $HOME/.vimrc
        ln -s $script_path/vimrc $HOME/.vimrc
    fi

# Linux
elif [[ $(uname -s) == Linux ]] ; then
    rm ~/.bashrc
    ln -s $script_path/bashrc $HOME/.bashrc

    if [[ $(command -v nvim) ]] ; then
        rm $HOME/.config/nvim/init.vim
        ln $script_path/vimrc $HOME/.config/nvim/init.vim
    else
        rm $HOME/.vimrc
        ln -s $script_path/vimrc $HOME/.vimrc
    fi
fi

This project has been migrated to [codeberg.org/diwesser/dotfiles](https://codeberg.org/diwesser/dotfiles)

# dotfiles
My public dotfiles

To Add:  

- [ ] tmux config
  - [ ] Status line git info, battery status, time, date
  - Links
    - [The Definitive Guide to Customizing the Tmux Status Line](https://medium.com/hackernoon/customizing-tmux-b3d2a5050207)
